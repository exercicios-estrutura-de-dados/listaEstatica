# Lista estática

Este é um pequeno programa em C que contém uma lista estática com
* Inserção de elemento na lista
* Remoção de elemento da lista
* Exibição dos elementos da lista

Este projeto foi proposto na disciplina de **Estrutura de Dados 1** no [**Instituto de Informática**](https://inf.ufg.br/) da [**Universidade Federal de Goiás**](https://www.ufg.br/).

## Como compilar e executar?

Os requisitos são:
* [Git](https://git-scm.com/)
* [GNU Make](https://www.gnu.org/software/make/)
* Um Compilador para **C**
    * [GCC](https://gcc.gnu.org/) ou
    * [Clang](https://clang.llvm.org/) ou
    * [MinGW](https://www.mingw-w64.org/)

### Instação das ferramentas
* No **Windows** utilizando [Chocolatey](https://chocolatey.org/install#individual). </br>
    Execute o comando no PowerShell
```powershell
> choco install git mingw make
```

* No **Ubuntu, Debiand, Mint, PopOS**...</br>
    Execute o comando no Terminal
```bash
$ sudo apt update && sudo apt install git build-essential
```

* No **MacOS** </br>
    Instale [Command Line Tools](https://developer.apple.com/download/all/) </br>
    ou [Xcode](https://developer.apple.com/download/all/)

### Compilando e executando
Basta clonar o repositório e utilizar o make para compilar
```bash
$ git clone https://gitlab.com/exercicios-estrutura-de-dados/listaEstatica.git
$ cd listaEstatica/
$ make
$ make run
```

## License
This project is licensed under the terms of the [MIT](https://choosealicense.com/licenses/mit/) license.
