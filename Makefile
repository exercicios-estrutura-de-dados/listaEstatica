# Variávies
CC = gcc
CFLAGS = -std=c99 -pedantic-errors -Wall

# Regra : dependências
all:
	$(CC) $(CFLAGS) listaEstatica.c -o listaEstatica

run:
	./listaEstatica

debug:
	make CFLAGS+=-g
