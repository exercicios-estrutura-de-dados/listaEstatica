#include <stdio.h>
#include <locale.h>
#define SIZE 10 // Tamanho do vetor
int listaNumerica[SIZE]; // vetor onde referente a lista
int codigoErro;
int primeiraExecucao=1;

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
    #include <stdlib.h>
#endif

int limparLista(int *listaNumerica){
        for(int i=0; i < SIZE; i++){ // Coloca zero em todas as posições do vetor
        listaNumerica[i] = 0;
    }
    return 0; // Sucesso ao limpar
}
int quantidadeItensNaLista(int *listaNumerica){
        int i, quantidadeItens=0;

    for(i=0; i <= SIZE; i++){ // Conta quantos itens diferente de 0
        if(listaNumerica[i] != 0){
            quantidadeItens++;
        };
    }
    return quantidadeItens;
}
int inserirElemento(int *listaNumerica, int numero, int posicao){
    if(quantidadeItensNaLista(listaNumerica) == SIZE){
        return -1; // Erro lista cheia
    }else if(posicao < 0 || posicao > SIZE -1){
        return -2; // Erro posição inválida
    };
    listaNumerica[posicao] = numero; // Insere número
    return 0; // Sucesso ao inserir
}
int removerElemento(int *listaNumerica, int posicao){
    if(quantidadeItensNaLista(listaNumerica) == 0){
        return -1; // Erro lista vazia
    }else if(posicao < 0 || posicao > SIZE -1){
        return -2; // Erro posição inválida
    };
    listaNumerica[posicao] = 0;
    return 0; // Sucesso ao remover
}
void listarElementos(int *listaNumerica){
    int i, j, proximoElemento;

    printf(
        "\n\033[7m"
        "+-----------+-----------+----------------+----------------+\n"
        "|  Posição  |  Número   | Próximo número | Pos. próx. num |\n"
        "+-----------+-----------+----------------+----------------+\033[0m\n"
    );
    for(i=0; i < SIZE; i++){ // Percorre o vetor principal
        j = i+1; // Apartir da próxima posição
        while(listaNumerica[j] == 0 && j < SIZE){ // Procura o próximo elemento
            j++;
        }
        if(j == SIZE){ // Se chegou em SIZE, percorreu o vetor e não encontrou o próximo
            printf(
                "|%10d |%10d | -------------- | -------------- | \n"
                "+-----------+-----------+----------------+----------------+ \n",
                i,
                listaNumerica[i]
            );
        }else{ // Caso contrário, encontrou o próximo valor
            proximoElemento = listaNumerica[j];
            printf(
                "|%10d |%10d |%15d |%15d | \n"
                "+-----------+-----------+----------------+----------------+ \n",
                i,
                listaNumerica[i],
                proximoElemento,
                j
            );
        }
    }
}
void limparTela(){
    printf("\033[H\033[2J\033[3J");
}
int main(){
    int opcaoEscolhida=0, numero=0, posicao=0;

    if(primeiraExecucao){
        #if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
            system("chcp 65001"); // Ativa UTF-8 encoding
            printf("\033[H\033[2J\033[3J");
            setlocale(LC_ALL, NULL);
        #else
            setlocale(LC_ALL,"Portuguese");
        #endif
        primeiraExecucao = 0;
    }

    printf(
        "\033[5;32m======== Menú principal ========== \033[0m \n"
        "1 - limpar lista \n"
        "2 - inserir elemento \n"
        "3 - remover elemento \n"
        "4 - listar elementos \n"
        "5 - sair \n"
        ">: "
    );
    scanf("%d", &opcaoEscolhida);
    while(getchar() != '\n'); // Limpa o buffer de StdIn

    switch(opcaoEscolhida){
        case 1:
            codigoErro = limparLista(listaNumerica);

            limparTela();
            if(codigoErro == 0){
                printf("A lista foi limpada com sucesso. \n");
            }
            main(); // Volta para main e mostra menú
            break;
        case 2:
            limparTela();
            printf("\033[5;32m===== Inserir elemento ===== \033[0m \n");
            printf("Numero: ");
            scanf("%d", &numero);
            printf("Posicao: ");
            scanf("%d", &posicao);
            codigoErro = inserirElemento(listaNumerica, numero, posicao);

            limparTela();
            if(codigoErro == 0){
                printf("Inserido com sucesso \n");
            }
            else if(codigoErro == -1){
                printf("Erro: a lista está cheia \n");
            }else{
                printf("Posição inválida! Tente novamente \n");
            }
            main(); // Volta para main e mostra menú
            break;
        case 3:
            limparTela();
            printf("\033[5;32m===== Remover elemento ===== \033[0m \n");
            printf("Posição do numero: ");
            scanf("%d", &posicao);
            codigoErro = removerElemento(listaNumerica, numero);

            limparTela();
            if(codigoErro == 0){
                printf("Removido com sucesso \n");
            }
            else if(codigoErro == -1){
                printf("Erro: a lista está vazia \n");
            }else{
                printf("Posição inválida! Tente novamente \n");
            }
            main(); // Volta para main e mostra menú
            break;
        case 4:
            limparTela();
            printf("\033[5;32m===== Listar elementos ===== \033[0m \n");
            listarElementos(listaNumerica);
            printf("\nPressione enter para continuar... \n");
            while(getchar() != '\n'); // Esperando "Enter" para continuar
            limparTela();
            main(); // Volta para main e mostra menú
            break;
        case 5:
            limparTela();
            printf("Saindo do programa...\n ");
            break; // Sai do switch, finalizando o programa
        default:
            limparTela();
            printf("Opçã inválida! Escolha novamente \n");
            main(); // Volta para main e mostra menú
    };
    return 0;
}
